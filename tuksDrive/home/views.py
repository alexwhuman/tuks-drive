from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.models import User
from home.models import TrashedFile, DeletionTimeFrame, SharedFolderUser
import os, shutil, datetime
from home.functions import *

def home(request, directory=''):
    if request.user.is_authenticated:
        if is_Valid_Directory(request, directory):
            current_url, current_filepath = current_Directory_Filepath(request, directory)
            folders_is_empty, files = empty_Folders_Files(current_filepath)
            files_sizes = zip(files, individual_file_sizes(current_filepath))
            back = back_path(directory)

            diskQuota = get_quota(request)
            if diskQuota != 'Unlimited':
                diskQuota = convert_size(diskQuota)

            return render(request, 'home/home.html', {
                'admin': is_admin(request),
                'directory': directory,
                'folders': folders_is_empty, 
                'files': files_sizes, 
                'back': back, 
                'trash_utilization': trash_utilization_percentage(request), 
                'storage_utilization': storage_utilization_percentage(request), 
                'total_utilization': trash_utilization_percentage(request) + storage_utilization_percentage(request),
                'disk_quota': diskQuota
            })
        else:
            return redirect('/')
    else:
        return login(request)

def trash(request):
    if request.method == 'POST':
        updated_time = request.POST['days']
        deletionTimeFrameObjects = DeletionTimeFrame.objects.filter(user_id=str(request.session['_auth_user_id']))
        user_time_frame = deletionTimeFrameObjects[0]
        user_time_frame.days_until_deletion = updated_time
        user_time_frame.save()
        return redirect('/Trash/')
    else:
        trashedFiles, userDeletionTimeframe = trashed_files(request)
        response = render(request, 'home/trash.html', {
            'trashed_files': trashedFiles, 
            'user_deletion_timeframe': userDeletionTimeframe
        })
        return response

def shared(request, shared_directory=''):

    sharedFoldersIndividuals = shared_with_me_by_individuals(request)
    sharedFoldersGroups = shared_with_my_groups(request)

    if shared_directory != '':
        if is_Shared_Directory(request, shared_directory):
            folders, files = folders_Files(shared_Directory(shared_directory))
            files = shared_file_directories(files, shared_directory)
            editable = is_editable_shared_directory(request, shared_directory)
            return render(request, 'home/shared.html', {
                'folders':folders, 
                'files':files, 
                'back':back_path(back_path(shared_directory)) + '/', 
                'editable': str(editable)
            })
        else:
            return redirect('/Shared/')
    
    return render(request, 'home/shared.html', {
        'shared_individuals':sharedFoldersIndividuals, 
        'shared_groups': sharedFoldersGroups,
        'shared_directory':shared_directory, 
    })

def share(request, path_to_directory=''):
    folder = file_directory(request, path_to_directory)
    folders, files = folders_Files(folder)

    users = User.objects.exclude(username=str(request.user))
    groups = Group.objects.all()

    if request.method == 'POST':
        if request.POST['group_individual'] == 'group':
            share_with_group(request, folder)
        else:
            share_with_individual(request, folder)
        return redirect('/')
    else:
        return render(request, 'home/share.html', {
            'admin': is_admin(request),
            'folder_to_share': folder_name(path_to_directory), 
            'folders': folders, 
            'files': files, 
            'users': users, 
            'groups': groups,
            'cancel': return_After_File_Operation(path_to_directory)
        })

def update_deletion_time(request):
    if request.method == 'POST':
        updated_time = request.POST['days']
        deletionTimeFrameObjects = DeletionTimeFrame.objects.filter(user_id=str(request.session['_auth_user_id']))
        user_time_frame = deletionTimeFrameObjects[0]
        user_time_frame.days_until_deletion = updated_time
        user_time_frame.save()
        return redirect('/Trash/')
    else:
        return render (request, 'home/update_deletion_time.html')

def download(request, path_to_file):
    requested_file_directory = file_directory(request, path_to_file)
    response = serve_file(requested_file_directory)
    return response

def shared_download(request, path_to_file):
    if is_Shared_Directory(request, file_location_directory(request, path_to_file)):
        requested_file_directory = shared_file_directory(path_to_file)
        response = serve_file(requested_file_directory)
        return response
    else:
        return redirect('/Shared/')

def send_to_trash(request, path_to_file):
    filename = file_name(path_to_file)
    original_directory = file_location_directory(request, path_to_file)

    shutil.copy(file_directory(request, path_to_file), trash_directory(request))

    trashed_file = TrashedFile.objects.create(user_id=str(request.session['_auth_user_id']), file_name=filename, date_trashed=(datetime.date.today().year, datetime.date.today().month, datetime.date.today().day), original_directory=original_directory)
    trashed_file.save()

    fs = FileSystemStorage()
    fs.delete(file_directory(request, path_to_file))

    return redirect(return_After_File_Operation(path_to_file))

def restore(request, original_path):

    shutil.copy(trashed_file_directory(request, original_path), restore_directory(request, original_path))

    TrashedFile.objects.filter(file_name=file_name(original_path)).delete()
    delete_file(trashed_file_directory(request, original_path))

    return redirect("/Trash/")


def delete(request, path_to_file):
    fileName = path_to_file
    delete_file(trashed_file_directory(request, path_to_file))

    TrashedFile.objects.filter(file_name=fileName).delete()

    return redirect("/Trash/")

def delete_directory(request, path_to_directory):
    fullpath = os.path.join(os.path.join(settings.MEDIA_ROOT, str(request.user)), path_to_directory)
    fs = FileSystemStorage() 
    fs.delete(fullpath)

    try:
        last_slash_pos = path_to_directory.rindex("/")
        directory = path_to_directory[0:last_slash_pos]
        return redirect("/" + directory)
    except:
        return redirect("home")

def upload(request, directory=''):
    if request.method == 'POST':
        try:
            uploaded_file = request.FILES['document']
            current_directory = os.path.join(os.path.join(settings.MEDIA_ROOT, str(request.user)), directory)
            fs = FileSystemStorage(current_directory)
            fs.save(uploaded_file.name, uploaded_file)
            return redirect('/' + str(directory))
        except:
            return redirect('/' + str(directory))
    else:
        return render(request, 'home/upload.html', {
            'directory':directory
        })

def create_folder(request, directory=""):
    if request.method == 'POST':
        current_url, current_filepath = current_Directory_Filepath(request, directory)
        path_to_new_directory = os.path.join(current_filepath, request.POST['folder_name'])
        try:
            os.mkdir(path_to_new_directory)
        except:
            return redirect('/' + directory)
        return redirect('/' + directory)
    else:
        return render(request, 'home/create_folder.html', {
            'directory': directory
        })

def login(request):
    if request.method == 'POST':
        user = authenticate(request, username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            auth_login(request, user)
            return redirect('/')
        else:
            return render(request, 'home/login.html', {
                'unsuccessful': True
            })   
        
    else:
        return render(request, 'home/login.html')

def signup(request):
    if request.method == 'POST':
        create_user(request, request.POST['username'], request.POST['password'])
        return redirect('/')
    else:
        return render(request, 'home/signup.html')

def logout(request):
    auth_logout(request)
    return redirect('home')

def copy(request, path_to_file, destination=''):
    userDirectory = user_directory(request)

    if destination == '':
        folders, files = folders_Files(userDirectory)
        full_destination = userDirectory
    else:
        path = os.path.join(userDirectory, destination)
        folders, files = folders_Files(path)
        full_destination = os.path.join(userDirectory, destination)

    if request.method == 'POST':
        shutil.copy(file_directory(request, path_to_file), full_destination)
        return redirect(return_After_File_Operation(path_to_file))
    else:
        return render(request, 'home/copy.html', {
            'fileName': file_name(path_to_file), 
            'folders': folders, 
            'path_to_file': path_to_file, 
            'destination': destination, 
            'back': back_path(destination), 
            'cancel': back_path(path_to_file)
        })

def move(request, path_to_file, destination=''):
    userDirectory = user_directory(request)

    if destination == '':
        folders, files = folders_Files(userDirectory)
        full_destination = userDirectory
    else:
        path = os.path.join(userDirectory, destination)
        folders, files = folders_Files(path)
        full_destination = os.path.join(userDirectory, destination)

    if request.method == 'POST':
        shutil.copy(file_directory(request, path_to_file), full_destination)
        delete_file(file_directory(request, path_to_file))
        return redirect(return_After_File_Operation(path_to_file))
    else:
        return render(request, 'home/move.html', {
            'fileName': file_name(path_to_file), 
            'folders': folders, 
            'path_to_file': path_to_file, 
            'destination': destination, 
            'back': back_path(destination), 
            'cancel': back_path(path_to_file)
        })

def rename(request, path_to_file):
    if request.method == 'POST':
        new_name = request.POST['new_name']
        new_fullpath = os.path.join(user_directory(request), back_path(path_to_file), new_name)
        try:
            os.rename(file_directory(request, path_to_file), new_fullpath)
        except:
            return redirect(return_After_File_Operation(path_to_file))

        return redirect(return_After_File_Operation(path_to_file))
    else:
        return render(request, 'home/rename.html', {
            'fileName': file_name(path_to_file), 
            'cancel': back_path(path_to_file)
        })

def manage_sharing(request):
    if request.method == 'POST':
        try:
            SharedFolderUser.objects.filter(folder_path=request.POST['unshare_individual']).delete()
        except:
            SharedFolderGroup.objects.filter(folder_path=request.POST['unshare_group']).delete()
        return redirect('/Manage_Sharing/')
    else:
        sharedFoldersInvidividuals = shared_with_other_individuals(request)
        sharedFoldersGroups = shared_with_other_groups(request)
        return render(request, 'home/manage_sharing.html', {
            'sharedFoldersIndividuals': sharedFoldersInvidividuals, 
            'sharedFoldersGroups': sharedFoldersGroups, 
            'isAdmin': is_admin(request)
        })

def admin_page(request):
    if is_admin(request):
        users = User.objects.all()
        groups = Group.objects.all()
        return render(request, 'home/admin_page.html', {
            'users': users, 
            'groups': groups
        })
    else:
        return redirect('/')

def admin_user_config(request, user):
    if request.method == 'POST':
        try:
            group = request.POST['add_to_group']
            add_to_group(User.objects.filter(username=user)[0], group)
        except:
            None
        try:
            new_quota = int(request.POST['new_quota'])
            if request.POST['size_suffix'] == 'gb':
                new_quota = new_quota * 1024 * 1024 * 1024
            elif request.POST['size_suffix'] == 'mb':
                new_quota = new_quota * 1024 * 1024
            set_quota(user, new_quota)
        except:
            None
        try:
            if request.POST['set_unlimited'] == 'True':
                set_quota(user, 'Unlimited')
        except:
            None
        try:
            remove_from_group(user, request.POST['group_to_remove'])
        except:
            None

    user_groups = get_groups_admin(user)
    all_groups = Group.objects.all()
    user_quota = get_quota_admin(user)
    return render(request, 'home/user_config.html', {
        'user': user, 
        'user_quota': user_quota, 
        'user_groups': user_groups, 
        'all_groups': all_groups
    })

def admin_add_group(request):
    if request.method == 'POST':
        new_group(request.POST['group_name'])
        return redirect('/Admin/')
    else:
        return render(request, 'home/add_group.html')

def admin_group_config(request, group):

    if request.method == 'POST':
        try:
            remove_from_group(request.POST['user_to_remove'], group)
        except:
            None    
        try:
            add_to_group(request.POST['add_user'], group)
        except:
            None

    users = get_users_in_group(group)
    all_users = User.objects.all()
    return render(request, 'home/group_config.html', {
        'group': group, 
        'users': users, 
        'all_users':all_users
    })

def shared_create_folder(request, directory):
    if request.method == 'POST':
        path_to_new_directory = os.path.join(settings.MEDIA_ROOT, directory, request.POST['folder_name'])
        try:
            os.mkdir(path_to_new_directory)
        except:
            return redirect('/Shared/' + directory + '/')
        return redirect('/Shared/' + directory + '/')
    else:
        return render(request, 'home/create_folder.html', {
            'directory': 'Shared/'+ directory + '/'
        })

def shared_upload(request, directory):
    if request.method == 'POST':
            uploaded_file = request.FILES['document']
            current_directory = os.path.join(settings.MEDIA_ROOT, directory)
            fs = FileSystemStorage(current_directory)
            fs.save(uploaded_file.name, uploaded_file)
            return redirect('/Shared/'+ directory + '/')
    else:
        return render(request, 'home/upload.html', {
            'directory': 'Shared/'+ directory + '/'
        })

def shared_rename(request, path_to_file):
    if request.method == 'POST':
        new_name = request.POST['new_name']
        new_fullpath = os.path.join(settings.MEDIA_ROOT, back_path(path_to_file), new_name)
        new_fullpath = new_fullpath.replace('/', '\\')
        old_fullpath = os.path.join(settings.MEDIA_ROOT, path_to_file)
        os.rename(old_fullpath, new_fullpath)
        return redirect('/Shared/' + back_path(path_to_file)+'/')
    else:
        return render(request, 'home/rename.html', {
            'fileName': file_name(path_to_file), 
            'cancel': 'Shared/' + back_path(path_to_file) + '/'
        })

def shared_delete(request, path_to_file):
    fullpath = os.path.join(settings.MEDIA_ROOT, path_to_file)
    delete_file(fullpath)
    return redirect('/Shared/' + back_path(path_to_file)+'/')


    