from django.db import models

# Create your models here.
class TrashedFile(models.Model):
    user_id = models.CharField(max_length=30)
    file_name = models.CharField(max_length=255)
    date_trashed = models.CharField(max_length=100)
    original_directory = models.CharField(max_length=255)

class DeletionTimeFrame(models.Model):
    user_id = models.CharField(max_length=30)
    days_until_deletion = models.CharField(max_length=30)

class SharedFolderUser(models.Model):
    shared_by = models.CharField(max_length=30)
    shared_with_user = models.CharField(max_length=30)
    folder_name = models.CharField(max_length=255)
    folder_path = models.CharField(max_length=255)
    folder_relative_path = models.CharField(max_length=255)
    editable = models.CharField(max_length=10)

class SharedFolderGroup(models.Model):
    shared_by = models.CharField(max_length=30)
    shared_with_group = models.CharField(max_length=30)
    folder_name = models.CharField(max_length=255)
    folder_path = models.CharField(max_length=255)
    editable = models.CharField(max_length=10)

class UserDiskQuota(models.Model):
    user_id = models.CharField(max_length=30)
    quota = models.CharField(max_length=255)

