import os, datetime, math
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.models import User, Group, Permission
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from home.models import TrashedFile, DeletionTimeFrame, SharedFolderUser, SharedFolderGroup, UserDiskQuota
from django.contrib.contenttypes.models import ContentType

# Logs the provided variable the console, includes hashes so it's easy to spot in the console
def log_To_Console(variable):
    print('#########################################')
    print(variable)
    print('#########################################')

# Takes an HTTP request
# Returns the full system path of the currently logged in user's home directory
# This is used for operations on the currently logged in users files or folders
def user_directory(request):
    return os.path.join(settings.MEDIA_ROOT, str(request.user))

# Takes an HTTP request
# Returns the full system path of the currently logged in user's Trash directory
# This is used for operations regarding the trashing or restoring of files
def trash_directory(request):
    return os.path.join(settings.MEDIA_ROOT, str(request.user), 'Trash')

# Takes an HTTP request
# Returns 2 arrays (files user has trashed, days left until file is automatically deleted), and the value that the user currently has set as the "days till deletion"
# This is used to display the files in the users Trash directory, how many days the individual files have left until they're deleted and the users current "days till deletion" setting value
def trashed_files(request):
    trashedFiles = TrashedFile.objects.filter(user_id=str(request.session['_auth_user_id']))
    deletionTimeFrameObjects = DeletionTimeFrame.objects.filter(user_id=str(request.session['_auth_user_id']))
    user_deletion_timeframe = int(deletionTimeFrameObjects[0].days_until_deletion)
    deletion_delta = datetime.timedelta(days=user_deletion_timeframe)
    days_remaining = []

    for trashedFile in trashedFiles:
        year, month, day = trashedFile.date_trashed[1:len(trashedFile.date_trashed)-1].split(',')
        date_trashed = datetime.date(int(year), int(month), int(day))
        deletion_date = date_trashed + deletion_delta
        if (deletion_date <= datetime.date.today()):
            delete_file(trashed_file_directory(request, trashedFile.file_name))
        else:
            time_remaining = deletion_date - datetime.date.today()
            time_remaining = str(time_remaining)[0:str(time_remaining).rindex(',')]
            days_remaining.append(time_remaining)

    trashed_files_with_deletion_date = zip(trashedFiles, days_remaining)
    return trashed_files_with_deletion_date, user_deletion_timeframe

# Takes an HTTP request and the current directory
# Returns an array with 2 values: (the path appended to the browsers URL to get to the current directory, The full system path of the current directory)
# This is used for file operations such as saving files or downloading them
def current_Directory_Filepath(request, directory):
    current_url = '/' + directory
    current_filepath = os.path.join(settings.MEDIA_ROOT, user_directory(request), directory)
    return current_url, current_filepath

# Takes a filepath
# Returns 2 arrays: (The folders found in the filepath, the files found in the file path)
# This is used to display the folders and files inside a directory the user has navigated to
def folders_Files(filepath):
    fs = FileSystemStorage()
    folders, files = fs.listdir(path=filepath)
    try:
        folders.remove('Trash')
    except:
        folders = folders
    return folders, files

# Takes a filepath
# Returns A linked list and an array: (The folders in the filepath, whether or not the folder is empty), the files in the filepath
# This is used to decide whether or not to display the "Delete folder" option on folders (users can only delete empty folders)
def empty_Folders_Files(filepath):
    fs = FileSystemStorage()
    folders, files = fs.listdir(path=filepath)
    try:
        folders.remove('Trash')
    except:
        folders = folders
    empty_folders = []
    for folder in folders:
        if fs.listdir(path=os.path.join(filepath, folder)) == ([],[]):
            empty_folders.append(True)
        else:
            empty_folders.append(False)

    return zip(folders, empty_folders), files

# Takes a filepath (specifically to a file)
# Returns the URL of the parent directory
# This is used to redirect the user after they're performed a file operation (such as copying, moving or renaming a file)
def return_After_File_Operation(path_to_file):
    try:
        last_slash_pos = path_to_file.rindex("/")
        directory = path_to_file[0:last_slash_pos]
        return "/" + directory
    except:
        return "/"

# Takes a filepath (specifically to a folder)
# Returns the URL of the parent directory
# This is used to redirect the user after they've performed a folder operaton (namely, creating a folder)
def return_After_Folder_Operation(path_to_file):
    directory = path_to_file[0:path_to_file.rindex("Create_Folder")]
    return "/" + directory

# Takes the current directory
# Returns the URL of the parent directory
# This is used for the "back" button, so the user can navigate to the previous directory
def back_path(directory):
    try:
        back = directory[0:directory.rindex('/')]
    except:
        back = ''
    return back

# Takes an HTTP request and the path to a file
# Returns the full system path to the file in the users home directory 
# This is used throughout the program to get the full system path for files 
def file_directory(request, file_directory):
    return os.path.join(settings.MEDIA_ROOT, str(request.user), file_directory)

# Takes the path to a file
# Returns the full system path to the file
# This is used to get the full system path to a shared file (a file not stored within the currently logged on users home directory)
def shared_file_directory(file_directory):
    return os.path.join(settings.MEDIA_ROOT, file_directory)

# Takes an HTTP request and the original path to a trashed file
# Returns the full system path to a file located in the trash directory
# This is used for the restoration and deletion of files in the trash directory
def trashed_file_directory(request, original_path):
    return os.path.join(trash_directory(request), file_name(original_path))

# Takes an HTTP request and the path to a file
# Returns the location of the file (The directory in which its located)
# This is used to get the location of files shared with the user and to save the original location of trashed files
def file_location_directory(request, file_directory):
    try:
        parent_directory = file_directory[0:file_directory.rindex('/')+1]
    except:
        parent_directory = "/"
    return parent_directory

# Takes the path to a file
# Returns the name of the file
# This is used throughout the program to seperate the filename from the filepath
def file_name(file_directory):
    try:
        filename = file_directory[file_directory.rindex('/')+1:len(file_directory)]
    except:
        filename = file_directory
    return filename

# Takes the path to a folder
# Returns the name of the folder
# This is used throughout the program to seperate the filename from the filepath
def folder_name(folder_directory):
    try:
        folderName = folder_directory[folder_directory.rindex("\\")+1:len(folder_directory)]
    except:
        folderName = folder_directory
    
    return folderName

# Takes the path to a file
# Returns an HTTP response with the file attached
# This is used to download files that users have uploaded
def serve_file(file_path):
    fp = open(file_path, 'rb')
    response = HttpResponse(fp.read())
    fp.close
    try:
        filename = file_path[file_path.rindex('/')+1:len(file_path)]
    except:
        filename = file_path
    response['Content-Disposition'] = 'attachment; filename=' + filename
    return response

# Takes the path to a file
# Returns nothing
# This is used to delete stored files
def delete_file(file_path):
    fs = FileSystemStorage()
    fs.delete(file_path)

# Takes an HTTP request and the original path to a trashed file
# Returns the full system path to which the file must be restored
# This is used in the restoration of trashed files
def restore_directory(request, original_path):
    original_path = original_path[0:original_path.rindex('/')+1]
    if original_path == '/':
        restoreDirectory = user_directory(request)
    else:
        restoreDirectory = os.path.join(user_directory(request), original_path)
    return restoreDirectory

# Takes an HTTP request, a username and a password
# Returns nothing
# This is used for the creation of users and their home directories, as well as setting the default value for their disk quota
def create_user(request, username, password):
    user = User.objects.create_user(username, '', password)
    auth_login(request, user)
    userDirectory = user_directory(request)
    trashDirectory = trash_directory(request)
    os.mkdir(userDirectory)
    os.mkdir(trashDirectory)
    deletion_time_frame = DeletionTimeFrame(user_id=str(request.session['_auth_user_id']), days_until_deletion='15')
    deletion_time_frame.save()
    disk_quota = UserDiskQuota(user_id=str(request.session['_auth_user_id']), quota=2147483648)
    disk_quota.save()
    
# Takes an HTTP request
# Returns an array of folders shared with the currently logged in user by other individuals: (SharedFolderUser objects, Names of the shared folders)
# This is used to display (and navigate and access) folder shared with the currently logged in user
def shared_with_me_by_individuals(request):
    sharedFolders = SharedFolderUser.objects.filter(shared_with_user=str(request.user))
    return sharedFolders

# Takes an HTTP request
# Returns an array of dictionaries of folders shared with the currently logged in users group(s), based on which group files are shared with: (dictionary:{'group name', 'shared folders'})
# This is used to display (and navigate and access) folders shared with the currently logged in user's groups
def shared_with_my_groups(request):
    user_groups = get_groups(request)
    sharedGroupFolders = []
    for group in user_groups:
        shared = SharedFolderGroup.objects.filter(shared_with_group=str(group))
        sharedNames = []
        for folder in shared:
            sharedNames.append(folder_name(folder.folder_path))
        sharedFolders = zip(shared, sharedNames)
        groups_and_shared = {
            'group': group, 
            'sharedFolders': sharedFolders
        }
        sharedGroupFolders.append(groups_and_shared)
    return sharedGroupFolders

# Takes an HTTP Request
# Returns all the folders the user is currently sharing with other users
# This is used to allow the user to manage their sharing (see what they're sharing with who)
def shared_with_other_individuals(request):
    sharedFolders = SharedFolderUser.objects.filter(shared_by=str(request.user))
    return sharedFolders

def shared_with_other_groups(request):
    sharedFolders = SharedFolderGroup.objects.filter(shared_by = str(request.user))
    return sharedFolders

# Takes an input
# Returns input
# This is used in combination with the Traverse Folders function (see said function for more details)
def return_string(input):
    return input

# Takes an HTTP request and the current directory
# Returns True or False 
# This is used to determine whether the directory the user has accessed is part of the directories shared with the user
def is_Shared_Directory(request, accessed_directory):
    validDirectories = []
    directoriesAndSubdirectories = []
    sharedInvididual = SharedFolderUser.objects.filter(shared_with_user=str(request.user))

    for group in get_groups(request):
        sharedGroup = SharedFolderGroup.objects.filter(shared_with_group=group)
        for folder in sharedGroup:
            for subfolder in traverse_folders(folder.folder_path, return_string, '').split(','):
                directoriesAndSubdirectories.append(subfolder)
            if folder.editable == 'True':
                for subfolder in traverse_folders(folder.folder_path, return_string, '').split(','):
                    directoriesAndSubdirectories.append(subfolder + '\\Upload')
                    directoriesAndSubdirectories.append(subfolder + '\\Create_Folder')
                    directoriesAndSubdirectories.append(subfolder + '\\Rename')
                    directoriesAndSubdirectories.append(subfolder + '\\Move')
                    directoriesAndSubdirectories.append(subfolder + '\\Copy')
    
    for folder in sharedInvididual:
        for subfolder in traverse_folders(folder.folder_path, return_string, '').split(','):
                directoriesAndSubdirectories.append(subfolder)
        if folder.editable == 'True':
                for subfolder in traverse_folders(folder.folder_path, return_string, '').split(','):
                    directoriesAndSubdirectories.append(subfolder + '\\Upload')
                    directoriesAndSubdirectories.append(subfolder + '\\Create_Folder')
                    directoriesAndSubdirectories.append(subfolder + '\\Rename')
                    directoriesAndSubdirectories.append(subfolder + '\\Move')
                    directoriesAndSubdirectories.append(subfolder + '\\Copy')
            
    
    for folder in directoriesAndSubdirectories:
        validDirectories.append(folder[len(settings.MEDIA_ROOT)+1:len(folder)] + '\\')

    accessed_directory = accessed_directory.replace('/', '\\')


    try: 
        validDirectories.index(accessed_directory)
        return True
    except: 
        return False

def is_editable_shared_directory(request, accessed_directory):
    validDirectories = []
    directoriesAndSubdirectories = []
    sharedInvididual = SharedFolderUser.objects.filter(shared_with_user=str(request.user), editable='True')

    for group in get_groups(request):
        sharedGroup = SharedFolderGroup.objects.filter(shared_with_group=group, editable='True')
        for folder in sharedGroup:
            for subfolder in traverse_folders(folder.folder_path, return_string, '').split(','):
                directoriesAndSubdirectories.append(subfolder)
    
    for folder in sharedInvididual:
        for subfolder in traverse_folders(folder.folder_path, return_string, '').split(','):
                directoriesAndSubdirectories.append(subfolder)
    
    for folder in directoriesAndSubdirectories:
        validDirectories.append(folder[len(settings.MEDIA_ROOT)+1:len(folder)] + '\\')

    accessed_directory = accessed_directory.replace('/', '\\')

    try: 
        validDirectories.index(accessed_directory)
        return True
    except: 
        return False


# Takes an HTTP request and the current directory
# Returns True or False
# This is used to determine whether or not the user is accessing a valid directory within their own folder structure
def is_Valid_Directory(request, accessed_directory):
    if accessed_directory != '':
        accessed_directory = '/' + accessed_directory
    validDirectories = traverse_folders(user_directory(request), return_string ,'')
    validDirectories = validDirectories.split(',')
    validDirectories.remove('')
    shortened = []

    for directory in validDirectories:
        directory = directory.replace('\\', '/')
        if directory[directory.rindex(str(request.user)):len(directory)] == str(request.user):
            shortened.append('')
        else:
            shortened.append(directory[directory.rindex(str(request.user))+len(str(request.user)):len(directory)])

    try:
        shortened.index(accessed_directory)
        return True
    except:
        return False

# Takes a shared URL
# Returns the full system path to said URL
# This is used for accessing files shared with the user
def shared_Directory(shared_Directory):
    return os.path.join(settings.MEDIA_ROOT, shared_Directory)

# Takes an array of files and a directory
# Returns a linked list of files joined with the provided directory
# This is used to join shared file names with the shared directory they're found in, for accessing said shared files
def shared_file_directories(shared_files, shared_directory):
    file_directories = []
    for file in shared_files:
        file_directories.append(os.path.join(shared_directory, file))
    return zip(shared_files, file_directories)

# Takes the path to a file
# Returns the size of the file
# This is used for displaying the sized of files
def files_size(path):
    size = 0
    folders, files = folders_Files(path)
    fs = FileSystemStorage(path)
    for file in files:
        size += fs.size(file)
    return size

# Takes an HTTP request
# Returns the percentage of the users disk quota that's occupied by Trashed Files
# This is used to show the user how much of their current disk quota is taken up by trashed files
def trash_utilization_percentage(request):
    user_quota = get_quota(request)
    if (user_quota == 'Unlimited'):
        return 'Unlimited'
    else:
        return round((files_size(trash_directory(request)) / user_quota) * 100, 2)

# Takes an HTTP Request
# Returns the percentage of the users disk quota that's occupied by files not found in the users trash
# This is used to show the user how much of their disk quota is being taken up by regular files
def storage_utilization_percentage(request):
    user_quota = get_quota(request)
    if (user_quota == 'Unlimited'):
        return 'Unlimited'
    else:
        size = traverse_folders(user_directory(request), files_size, 0)
        return round((size/2147483648) * 100, 2)


# Takes a filepath starting point, a function, and a blank value of specified type (e.g. '' for a string, 0 for an int etc.)
# Returns the result of runnning the specified function in every directory and child directory of the provided file path
# This is used in various places thoughout the program, for example to get the sizes of all folders
def traverse_folders(start_point, function, value):
    if isinstance(function(start_point), int):
        value = int(value)
        value += function(start_point)
    elif isinstance(function(start_point), str):
        value = str(value)
        value += function(start_point) + ','

    folders, files = folders_Files(start_point)

    if folders == []:
        return value
    else:
        for folder in folders:
            value = traverse_folders(os.path.join(start_point, folder), function, value)
        return value

# Takes a directory
# Returns an array of the sizes of the files in the directory
# This is used to display the size of individual files as well calculating how much storage the user is utilising
def individual_file_sizes(directory):
    fs = FileSystemStorage(directory)
    folders, files = folders_Files(directory)
    sizes = []
    for file in files:
        sizes.append(convert_size(fs.size(file)))
    return sizes

# Takes file size in bytes
# Returns file size in either GB or MB
# This is used to convert the file sizes into human-readable values
def convert_size(size):
    size = round(size/1024/1024, 2)
    if size/1024 >= 1:
        size = round(size/1024, 2)
        size = str(size) + ' GB'
    else:
        size = str(size) + ' MB'
    return size

# Takes a user and a group
# Returns nothing
# This is used to add users to groups
def add_to_group(user, group):
    my_group = Group.objects.get(name=group)
    my_user = User.objects.get(username=user)
    my_group.user_set.add(my_user)
    return

# Takes a user and a group
# Returns nothing
# This is used to remove a user from a group
def remove_from_group(user, group):
    my_group = Group.objects.get(name=group)
    my_user = User.objects.get(username=user)
    my_group.user_set.remove(my_user)
    return

# Takes the name of a group
# Returns nothing
# This is used to create new groups
def new_group(group):
    Group.objects.get_or_create(name=group)
    return

# Takes an HTTP request and a folder path
# Returns nothing
# This is used to share a folder with another user
def share_with_individual(request, folder_path):
    shared_with = request.POST['share_to_user']
    try:
        editPermissions = request.POST['edit_permissions']
    except:
        editPermissions = False
    if len(SharedFolderUser.objects.filter(shared_by=str(request.user), shared_with_user=shared_with, folder_path=folder_path.replace('/', '\\'), folder_name=folder_name(folder_path), folder_relative_path=folder_relative_path(request.user, folder_path))) == 0:
        shared_folder = SharedFolderUser.objects.create(shared_by=str(request.user), shared_with_user=shared_with, folder_path=folder_path.replace('/', '\\'), folder_name=folder_name(folder_path), folder_relative_path=folder_relative_path(request.user, folder_path) , editable=editPermissions)
        shared_folder.save()
    return

# Takes an HTTP request and a folder path
# Returns nothing
# This is used to share a folder with a group
def share_with_group(request, folder_path):
    shared_with = request.POST['share_to_group']
    try:
        editPermissions = request.POST['edit_permissions']
    except:
        editPermissions = False
    if len(SharedFolderGroup.objects.filter(shared_by=str(request.user), shared_with_group=shared_with, folder_path=folder_path, folder_name=folder_name(folder_path))) == 0:
        shared_folder = SharedFolderGroup.objects.create(shared_by=str(request.user), shared_with_group=shared_with, folder_path=folder_path, folder_name=folder_name(folder_path), editable=editPermissions)
        shared_folder.save()
    return 

# Takes an HTTP request
# Returns True or False
# This is used to verify whether or not the currently logged in user has administrator priveleges
def is_admin(request):
    user = User.objects.get(username = request.user)
    admin = Group.objects.get(name='admin')
    if user.groups.filter(name=admin).exists():
        return True
    else:
        return False

# Takes an HTTP Request
# Returns an array of groups
# This is used to check what groups a user belongs to 
def get_groups(request):
    return request.user.groups.all()

# Takes a username
# Returns an array of groups
# This is used to check what groups a provided user is in (for administative user management purposes)
def get_groups_admin(username):
    user = User.objects.filter(username=username)[0]
    return user.groups.all()

# Takes a username
# Returns the user's disk quota
# This is used to get the disk quota of a specific user (for administrative user management purposes)
def get_quota_admin(username):
    user = User.objects.filter(username=username)[0]
    user_id = user.id
    user_quota = UserDiskQuota.objects.filter(user_id=user_id)[0].quota
    if user_quota == 'Unlimited':
        return 'Unlimited'
    else:
        return convert_size(int(user_quota))

# Takes an HTTP request
# Returns the currently logged in user's disk quota
# This is used to get the currently logged in user's disk quota
def get_quota(request):
    user = User.objects.filter(username=str(request.user))[0]
    user_id = user.id
    user_quota = UserDiskQuota.objects.filter(user_id=user_id)[0].quota
    if user_quota == 'Unlimited':
        return 'Unlimited'
    else:
        return int(user_quota)


# Takes a username and a disk quota (in bytes)
# Returns nothing
# This is used to set a specific users disk quota (for administrative user management purposes)
def set_quota(username, value):
    user = User.objects.filter(username=username)[0]
    user_quota = UserDiskQuota.objects.filter(user_id=user.id)[0]
    user_quota.quota = value
    user_quota.save()
    return

# Takes a group
# Returns a list of users
# This is used to see what users belong to a certain group (for administrative user management purposes)
def get_users_in_group(groupname):
    group = Group.objects.filter(name=groupname)[0]
    users = []
    for user in User.objects.all():
        if user.groups.filter(name=group.name).exists():
            users.append(user)
    return users

def folder_relative_path(user, folder_path):
    return folder_path[folder_path.rindex(str(user))+len(str(user))+1:len(folder_path)]
    












                









