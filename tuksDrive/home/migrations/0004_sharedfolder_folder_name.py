# Generated by Django 2.2.2 on 2019-06-21 17:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0003_sharedfolder'),
    ]

    operations = [
        migrations.AddField(
            model_name='sharedfolder',
            name='folder_name',
            field=models.CharField(default='lol', max_length=255),
            preserve_default=False,
        ),
    ]
